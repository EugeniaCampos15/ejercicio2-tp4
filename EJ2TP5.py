import os
from operator import itemgetter, attrgetter
def menu():
   os.system('cls')
   print('****************************************************') 
   print('**********MENU DE OPCIONES**********')
   print('1- Agregar vehiculos.')
   print('2- Mostrar lista de vehiculos.')
   print('3- Reservar un automovil.')
   print('4- Buscar un automovil por su dominio')
   print('5- Ordenar la lista de automoviles en forma ascendente o descendente por marca.') 
   print('6- Ordenar la lista de automoviles en forma ascendente o descendente por precio de venta, mostrando solamente los que se encuentren disponibles')
   print('7- Salir')
   op = int(input('Ingrese una opción: '))
   while not(op >= 1 and op <= 7): 
        op = int(input('Ingrese una opción: '))
   return op

def pulsarTecla():
    input('Pulse cualquier tecla para continuar...')

def ingreseDominio():
   dominio = input()
   dominio= dominio.replace(" ", "")
   while not(len(dominio)>=6 and len(dominio)<=9):
            dominio= input('Ingrese nuevamente el dominio ')
            dominio= dominio.replace(" ", "")
   print(dominio)
   return dominio
  
def validarDominio(vehiculos,dominio):
    valido = True
    for item in vehiculos:
        if dominio == item[0]:
            valido = False
            break
    return valido 

def ingreseMarca():
   marca = input()
   while not (marca in ['R','r', 'F','f', 'C', 'c']):
      marca= input ('Ingrese la marca: R=Renault, F=Ford, C=Citroen: ')
   if marca=='R' or marca== 'r':
       marca= 'Renault'
   elif marca == 'F' or marca== 'f':
       marca = 'Ford'
   else:
       marca = 'Citroen' or marca== 'c'
   return marca 


def ingreseTipo():
   tipo= input()
   while not (tipo in ['A','a','U','u' ]):
      tipo= input ('Ingrese tipo de vehiculo: U=Utilitario, A=Automóvil: ')
   if tipo == 'U' or 'u':
        tipo = 'Utilitarios'
   elif tipo== 'A' or 'a':
        tipo = 'Automovil'  
   return tipo
     
def ingreseModelo():
    modelo = int(input())
    while not(modelo>=2005 and modelo<=2020):
        modelo = int(input('Ingrese el modelo [2005-2020]: '))
    return modelo

def ingreseKm():
    kilometraje= int(input()) 
    while not(kilometraje >= 0):
        kilometraje = int(input('Ingrese el kilometraje: '))
    return kilometraje

def ingresePrecioVal():
   precioval=int(input())   
   while not(precioval >= 10000):
        precioval = int(input('Ingrese el precio valuado: '))
   return precioval

def precioVenta(precioval):
  preciovent = int(precioval*1.1)
  print(preciovent)

def mostrarvehiculos(lista):
    print('Listado de Automoviles')
    for item in lista:
        print(item)


def registroAutomoviles(vehiculos):
   print('***********Registro de su automovil***********')
   vehiculos  = [['AB101ED', 'Renault', 'Automovil', 2019, 20000, 250000, 275000, 'D'],
                 ['AC245EV', 'Ford', 'Utilitario', 2006, 80000, 150000, 175000,'D'],
                 ['AA170ED', 'Citroen', 'Automovil', 2019, 20000, 350000, 400000, 'D'],
                 ['AE205EV', 'Ford', 'Utilitario', 2006, 80000, 150000, 175000,'D'],
                 ['AB1201ED', 'Renault', 'Automovil', 2019, 20000, 250000, 275000, 'D'],
                 ['AC100EV', 'Ford', 'Utilitario', 2006, 80000, 150000, 175000,'D'],
                 ['AA102ED', 'Citroen', 'Automovil', 2019, 20000, 350000, 400000, 'D'],
                 ['AE206EV', 'Ford', 'Utilitario', 2006, 80000, 150000, 175000,'D'],
                 ['AE350EV', 'Renault', 'Utilitario', 2006, 80000, 150000, 175000,'D'],
                 ['AE063EV', 'Citroen', 'Automovil', 2006, 80000, 150000, 175000,'D']
                ]
   respuesta = 's'   
   while (respuesta=='s') or (respuesta=='S'):
      print('Ingrese el dominio: ') 
      dominio = ingreseDominio()
      if  validarDominio(vehiculos,dominio):
         print('Ingrese la marca: R=Renault, F=Ford, C=Citroen')
         marca = ingreseMarca()
         print('Ingrese tipo de vehiculo: U=Utilitario, A=Automóvil ')
         tipo = ingreseTipo()
         print('Ingrese el modelo [2005-2020]: ')
         modelo = ingreseModelo()
         print('Ingrese el kilometraje: ')
         kilometraje = ingreseKm()
         print('Ingrese el precio valuado: ')
         precioval = ingresePrecioVal()
         preciovent = int(precioval *1.1)  
         estado = 'D'
         vehiculos.append([dominio,marca,tipo,modelo,kilometraje,precioval,preciovent, estado])
         print('Agregado correctamente!')
      else:
         print('Dominio Existente! ')
      respuesta = input('Desea continuar cargando vehiculos? s/n: ')       
   return vehiculos

def buscar(vehiculos,dominio):
    pos = -1
    for i, item in enumerate(vehiculos):
        if(item[0] == dominio):
            pos = i
            break
    return pos  

def buscarPorDominio(vehiculos):
    print('***************Busqueda de Automovil*********************')
    dominio = input('Ingrese dominio para buscar: ')
    pos = buscar(vehiculos,dominio)
    if (pos != -1):
        print(vehiculos[pos])
    else:
        print('Vehiculo Inexistente')

def reservarAutomovil(vehiculos):
   print('Ingrese automovil para reservar: ')
   dominio = input('Ingrese Dominio: ')
   pos = buscar(vehiculos,dominio)
   if (pos != -1) and ((vehiculos[pos][7] != 'R') or (vehiculos[pos][7]=='V')):
        vehiculos[pos][7] = 'R'
        print('Vehiculo Reservado con exito')
   else:
        print('Automovil Inexistente o ya se encuentra Reservado o Vendido')
 

def ordenarPorMarca(vehiculos):
   print('Ordenar por marca de vehiculo')
   resp= int(input(' Ingrese la forma en que desea ordenar la lista: 1=Forma Ascendente, 2= Forma Descendente '))
   if resp==1:
      listaOrdenada = sorted(vehiculos,key=itemgetter(1))
      mostrarvehiculos(listaOrdenada)
   elif resp==2 :  
      listaOrdenada = sorted(vehiculos,key=itemgetter(1), reverse=True)
      mostrarvehiculos(listaOrdenada)
        
def  mostrarDisponibles(lista):
    print('Listado de Automoviles Disponibles')
    for item in lista:
        if item[7] =='D':
            print(item) 

def ordenarPorPrecioVenta(vehiculos):
   print('Ordenar por precio de venta de cada vehiculo')
   resp= int(input(' Ingrese la forma en que desea ordenar la lista: 1=Forma Ascendente, 2= Forma Descendente '))
   if resp==1:
      listaOrdenada = sorted(vehiculos,key=itemgetter(6)) 
      mostrarDisponibles(listaOrdenada)
   elif resp==2 :  
      listaOrdenada = sorted(vehiculos,key=itemgetter(6), reverse=True) 
      mostrarDisponibles(listaOrdenada)  
  

        
#Principal
op = 0
vehiculos = []
while op != 7:
    op = menu()
    if op == 1: 
        vehiculos= registroAutomoviles(vehiculos)
        pulsarTecla()
    elif op ==2: 
        mostrarvehiculos(vehiculos)
        pulsarTecla()
    elif op == 3: 
          reservarAutomovil(vehiculos)
          pulsarTecla()
    elif op == 4: 
        buscarPorDominio(vehiculos)
        pulsarTecla()
    elif op == 5:
        ordenarPorMarca(vehiculos)
        pulsarTecla()
    elif op == 6: 
       ordenarPorPrecioVenta(vehiculos)
       pulsarTecla()
    elif op == 7:
        print('EJECUCION FINALIZADA')
